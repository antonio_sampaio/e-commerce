package br.com.absn.wrapper;

/**
 * Created by asampaio on 02/04/17.
 */
public class CreditCardWrapper {

    private String hash;

    private HolderWrapper holderWrapper;

    public CreditCardWrapper(String hash, HolderWrapper holderWrapper) {
        this.hash = hash;
        this.holderWrapper = holderWrapper;
    }

    public String getHash() {
        return hash;
    }

    public HolderWrapper getHolderWrapper() {
        return holderWrapper;
    }
}
