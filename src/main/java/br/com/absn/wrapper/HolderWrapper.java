package br.com.absn.wrapper;

import java.util.Date;

/**
 * Created by asampaio on 02/04/17.
 */
public class HolderWrapper {

    private Integer installmentCount;
    private PhoneWrapper phoneWrapper;
    private String fullName;
    private Date birthdate;
    private Long cpf;

    public HolderWrapper(Integer installmentCount, PhoneWrapper phoneWrapper, String fullName, Date birthdate, Long cpf) {
        this.installmentCount = installmentCount;
        this.phoneWrapper = phoneWrapper;
        this.fullName = fullName;
        this.birthdate = birthdate;
        this.cpf = cpf;
    }

    public PhoneWrapper getPhoneWrapper() {
        return phoneWrapper;
    }

    public String getFullName() {
        return fullName;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public Long getCpf() {
        return cpf;
    }
}
