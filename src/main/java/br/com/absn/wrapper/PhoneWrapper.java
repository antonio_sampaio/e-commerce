package br.com.absn.wrapper;

/**
 * Created by asampaio on 20/03/17.
 */
public class PhoneWrapper {

    private Integer areaCode;
    private Integer number;

    public PhoneWrapper(int areaCode, int number) {
        this.areaCode = areaCode;
        this.number = number;
    }

    public Integer getAreaCode() {
        return areaCode;
    }

    public Integer getNumber() {
        return number;
    }
}
