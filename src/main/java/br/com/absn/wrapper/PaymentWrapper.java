package br.com.absn.wrapper;

/**
 * Created by asampaio on 02/04/17.
 */
public class PaymentWrapper {

    private String orderId;
    private Integer installmentCount;

    private CreditCardWrapper creditCardWrapper;

    public PaymentWrapper(String orderId, Integer installmentCount, CreditCardWrapper creditCardWrapper) {
        this.orderId = orderId;
        this.installmentCount = installmentCount;
        this.creditCardWrapper = creditCardWrapper;
    }

    public String getOrderId() {
        return orderId;
    }

    public Integer getInstallmentCount() {
        return installmentCount;
    }

    public CreditCardWrapper getCreditCardWrapper() {
        return creditCardWrapper;
    }



}
