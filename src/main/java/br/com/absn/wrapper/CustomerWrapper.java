package br.com.absn.wrapper;

import java.util.Date;

/**
 * Created by asampaio on 20/03/17.
 */
public class CustomerWrapper {

    private String ownId;
    private String fullName;
    private String email;
    private Date birthdate;
    private Long cpf;
    private PhoneWrapper phone;
    private ShippingAddressWrapper shippingAddress;

    public CustomerWrapper(String ownId, String fullName, String email, Date birthdate, Long cpf, PhoneWrapper phone, ShippingAddressWrapper shippingAddress) {
        this.ownId = ownId;
        this.fullName = fullName;
        this.email = email;
        this.birthdate = birthdate;
        this.cpf = cpf;
        this.phone = phone;
        this.shippingAddress = shippingAddress;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public String getTaxDocumentStr() {
        return cpf.toString();
    }

    public PhoneWrapper getPhone() {
        return phone;
    }

    public ShippingAddressWrapper getShippingAddress() {
        return shippingAddress;
    }
}
