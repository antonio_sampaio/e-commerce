package br.com.absn.wrapper;

import br.com.absn.model.Item;

import java.math.BigDecimal;

/**
 * Created by asampaio on 26/03/17.
 */
public class ItemCartWrapper extends Item{

    private Integer quantity;

    public ItemCartWrapper(Long id, String name, String description, BigDecimal price, Integer quantity) {
        super(id, name, description, price);
        this.quantity = quantity;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
