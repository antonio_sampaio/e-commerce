package br.com.absn.wrapper;

/**
 * Created by asampaio on 20/03/17.
 */
public class ShippingAddressWrapper {

    private String street;
    private Integer streetNumber;
    private String complement;
    private String city;
    private String state;
    private String district;
    private String country;
    private Integer zipCode;

    public ShippingAddressWrapper(String street, Integer streetNumber, String complement, String city, String state, String district, String country, Integer zipCode) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.complement = complement;
        this.city = city;
        this.state = state;
        this.district = district;
        this.country = country;
        this.zipCode = zipCode;
    }

    public String getStreet() {
        return street;
    }

    public String getStreetNumberStr() {
        return streetNumber.toString();
    }

    public String getComplement() {
        return complement;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getDistrict() {
        return district;
    }

    public String getCountry() {
        return country;
    }

    public String getZipCodeStr() {
        return zipCode.toString();
    }
}
