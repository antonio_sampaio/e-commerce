package br.com.absn.wrapper;

import java.util.List;

/**
 * Created by asampaio on 18/03/17.
 */
public class OrderWrapper {

    private String ownId;
    private CartWrapper cartWrapper;
    private CustomerWrapper customer;

    public OrderWrapper(String ownId, CartWrapper cartWrapper, CustomerWrapper customer) {
        this.ownId = ownId;
        this.cartWrapper = cartWrapper;
        this.customer = customer;
    }

    public CartWrapper getCartWrapper() {
        return cartWrapper;
    }

    public CustomerWrapper getCustomer() {
        return customer;
    }




}
