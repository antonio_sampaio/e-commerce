package br.com.absn.wrapper;

import java.util.List;

/**
 * Created by asampaio on 01/04/17.
 */
public class CartWrapper {

    private List<ItemCartWrapper> itens;

    public CartWrapper(List<ItemCartWrapper> itens) {
        this.itens = itens;
    }

    public List<ItemCartWrapper> getItens() {
        return itens;
    }
}
