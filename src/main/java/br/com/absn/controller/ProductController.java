package br.com.absn.controller;

import br.com.absn.model.Item;
import br.com.absn.service.component.session.CartSession;
import br.com.absn.stub.CouponStubRepository;
import br.com.absn.stub.ItemStubRepository;
import br.com.absn.wrapper.ItemCartWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by asampaio on 18/03/17.
 */

@Controller
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class ProductController {

    @Autowired
    private CartSession cartSession;

    @Autowired
    private ItemStubRepository itemRepository;

    @Autowired
    private ItemStubRepository itemStubRepository;

    @Autowired
    private CouponStubRepository couponRepository;


    @RequestMapping("index")
    public ModelAndView index(){
        Iterable<Item> Itens = itemStubRepository.findAll();

        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("Itens", Itens);

        return modelAndView;
    }

    @RequestMapping("single")
    public ModelAndView single(Long id){

        Item item = itemStubRepository.findOne(id);

        ModelAndView modelAndView = new ModelAndView("single");
        modelAndView.addObject("Item", item);

        return modelAndView;
    }

    @RequestMapping("add")
    public ModelAndView add(@RequestParam("id") String id, @RequestParam("quantity") Integer quantity){
        ModelAndView modelAndView = new ModelAndView("added_cart");
        cartSession.addItemById(Long.parseLong(id), quantity);

        return modelAndView;
    }

    @RequestMapping("checkout")
    public ModelAndView checkout(){

        ModelAndView modelAndView = new ModelAndView("checkout");
        List<ItemCartWrapper> itensCart = cartSession.findAll();
        modelAndView.addObject("itensCart", itensCart);

        return modelAndView;
    }

    @RequestMapping("finish")
    public ModelAndView finish(){
        ModelAndView modelAndView = new ModelAndView("finish_checkout");

        return modelAndView;
    }

    @RequestMapping("coupon")
    public ModelAndView coupon(String code){

        ModelAndView modelAndView = new ModelAndView("coupon");
        modelAndView.addObject("isCupomValid", couponRepository.isCupomValid(code));

        return modelAndView;
    }
}
