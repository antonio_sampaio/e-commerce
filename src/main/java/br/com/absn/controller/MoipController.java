package br.com.absn.controller;

import br.com.absn.service.OrderService;
import br.com.absn.service.PaymentService;
import br.com.absn.service.component.session.CartSession;
import br.com.absn.wrapper.OrderWrapper;
import br.com.absn.wrapper.PaymentWrapper;
import br.com.moip.resource.Order;
import br.com.moip.resource.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by asampaio on 03/04/17.
 */

@Controller
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class MoipController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private CartSession cartSession;

    @RequestMapping("order")
    public ModelAndView create(
            @RequestParam("full_name") String fullname,
            @RequestParam("email") String email,
            @RequestParam("birth_date") String birthDate,
            @RequestParam("cpf") String cpf,
            @RequestParam("phone_area_code") String phoneAreaCode,
            @RequestParam("phone_number") String phoneNumber,
            @RequestParam("street") String street,
            @RequestParam("street_number") String streetNumber,
            @RequestParam("complement") String complement,
            @RequestParam("district") String district,
            @RequestParam("city") String city,
            @RequestParam("state") String state,
            @RequestParam("country") String country,
            @RequestParam("zip_code") String zipCode) throws Exception {

        OrderWrapper orderWrapper = orderService.makeOrderWrapper(
                fullname,
                email,
                birthDate,
                cpf,
                phoneAreaCode,
                phoneNumber,
                street,
                streetNumber,
                complement,
                district,
                city,
                state,
                country,
                zipCode);

        Order order = orderService.createOrder(orderWrapper);

        ModelAndView modelAndView = new ModelAndView("payment");
        modelAndView.addObject("order", order);

        return modelAndView;
    }


    @RequestMapping("payment")
    public ModelAndView payment(
            @RequestParam("full_name") String fullname,
            @RequestParam("installment_count") Integer installmentCount,
            @RequestParam("birth_date") String birthDate,
            @RequestParam("phone_area_code") String phoneAreaCode,
            @RequestParam("phone_number") String phoneNumber,
            @RequestParam("cpf") String cpf,
            @RequestParam("id") String orderId,
            @RequestParam("encrypted_value") String encryptedValue) throws Exception {

        PaymentWrapper paymentWrapper = paymentService.makePaymentWrapper(
                fullname,
                installmentCount,
                birthDate,
                phoneAreaCode,
                phoneNumber,
                cpf,
                orderId,
                encryptedValue);

        cartSession.cleanCart();

        Payment payment = paymentService.createPayment(paymentWrapper);

        ModelAndView modelAndView = new ModelAndView("success");
        modelAndView.addObject("payment", payment);

        return modelAndView;
    }


}
