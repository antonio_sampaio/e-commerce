package br.com.absn.service;

import br.com.absn.wrapper.CustomerWrapper;
import br.com.moip.request.ApiDateRequest;
import br.com.moip.request.CustomerRequest;
import br.com.moip.request.TaxDocumentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by asampaio on 26/03/17.
 */

@Service
public class CustomerService {

    @Autowired
    private PhoneService phoneService;

    @Autowired
    private ShippingAddressService shippingAddressService;

    public CustomerRequest makeCustomerRequest(CustomerWrapper customerWrapper) throws Exception {

        new Date();

        return new CustomerRequest()
                .ownId("1")
                .fullname(customerWrapper.getFullName())
                .email(customerWrapper.getEmail())
                .birthdate(new ApiDateRequest().date(customerWrapper.getBirthdate()))
                .taxDocument(TaxDocumentRequest.cpf(customerWrapper.getTaxDocumentStr()))
                .phone(phoneService.makePhoneRequest(customerWrapper))
                .shippingAddressRequest(shippingAddressService.makeShippingAddressRequest(customerWrapper));
    }
}
