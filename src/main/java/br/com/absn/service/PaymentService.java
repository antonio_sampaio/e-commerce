package br.com.absn.service;

import br.com.absn.wrapper.CreditCardWrapper;
import br.com.absn.wrapper.HolderWrapper;
import br.com.absn.wrapper.PaymentWrapper;
import br.com.absn.wrapper.PhoneWrapper;
import br.com.moip.request.*;
import br.com.moip.resource.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by asampaio on 18/03/17.
 */

@Service
public class PaymentService {

    @Autowired
    private MoipService moipService;

    @Autowired
    FundingInstrumentService fundingInstrumentService;

    public Payment createPayment(PaymentWrapper paymentWrapper) throws Exception {
        PaymentRequest paymentRequest = makePaymentRequest(paymentWrapper);

        return moipService.createPayment(paymentRequest);
    }

    private PaymentRequest makePaymentRequest(PaymentWrapper paymentWrapper) throws Exception {

        FundingInstrumentRequest fundingInstrumentRequest
                = fundingInstrumentService.makeFundingInstrument(paymentWrapper);


        return new PaymentRequest()
                .installmentCount(paymentWrapper.getInstallmentCount())
                .orderId(paymentWrapper.getOrderId())
                .fundingInstrument(fundingInstrumentRequest);
    }

    public PaymentWrapper makePaymentWrapper(
            String fullname,
            Integer installmentCount,
            String birthDate,
            String phoneAreaCode,
            String phoneNumber,
            String cpf,
            String orderId,
            String encryptedValue) throws ParseException {

        PhoneWrapper phoneWrapper = new PhoneWrapper(Integer.parseInt(phoneAreaCode), Integer.parseInt(phoneNumber));

        HolderWrapper holderWrapper = new HolderWrapper(installmentCount,
                phoneWrapper,
                fullname,
                new SimpleDateFormat("yyyy-MM-dd").parse(birthDate),
                Long.parseLong(cpf));

        CreditCardWrapper creditCardWrapper = new CreditCardWrapper(
                encryptedValue,
                holderWrapper);

        return new PaymentWrapper(orderId, installmentCount, creditCardWrapper);
    }
}
