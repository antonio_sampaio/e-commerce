package br.com.absn.service;

import br.com.absn.wrapper.CustomerWrapper;
import br.com.moip.request.ShippingAddressRequest;
import org.springframework.stereotype.Service;

/**
 * Created by asampaio on 01/04/17.
 */

@Service
public class ShippingAddressService {

    public ShippingAddressRequest makeShippingAddressRequest(CustomerWrapper customerWrapper){
        return new ShippingAddressRequest()
                .street(customerWrapper.getShippingAddress().getStreet())
                .streetNumber(customerWrapper.getShippingAddress().getStreetNumberStr())
                .complement(customerWrapper.getShippingAddress().getComplement())
                .district(customerWrapper.getShippingAddress().getDistrict())
                .city(customerWrapper.getShippingAddress().getCity())
                .state(customerWrapper.getShippingAddress().getState())
                .country(customerWrapper.getShippingAddress().getCountry())
                .zipCode(customerWrapper.getShippingAddress().getZipCodeStr());
    }
}
