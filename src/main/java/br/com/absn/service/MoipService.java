package br.com.absn.service;

import br.com.absn.config.MoipApi;
import br.com.absn.wrapper.OrderWrapper;
import br.com.moip.API;
import br.com.moip.request.*;
import br.com.moip.resource.Order;
import br.com.moip.resource.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by asampaio on 26/03/17.
 */

@Service
public class MoipService {

    @Autowired
    private MoipApi moip;

    public Order createOrder(OrderRequest orderRequest){
        return moip.getApi().order().create(orderRequest);

//        return moip.getApi().order().create(new OrderRequest()
//                .ownId("order_own_id")
//                .addItem("Nome do produto", 1, "Mais info...", 100)
//                .customer(new CustomerRequest()
//                        .ownId("customer_own_id")
//                        .fullname("Jose da Silva")
//                        .email("josedasilva@email.com")
//                        .birthdate(new ApiDateRequest().date(new Date()))
//                        .taxDocument(TaxDocumentRequest.cpf("22222222222"))
//                        .phone(new PhoneRequest().setAreaCode("11").setNumber("55443322"))
//                        .shippingAddressRequest(new ShippingAddressRequest().street("Avenida Faria Lima")
//                                .streetNumber("3064")
//                                .complement("12 andar")
//                                .city("São Paulo")
//                                .state("SP")
//                                .district("Itaim")
//                                .country("BRA")
//                                .zipCode("01452-000")
//                        )
//                )
//        );

    }

    public Payment createPayment(PaymentRequest paymentRequest){
        return moip.getApi().payment().create(paymentRequest);
    }
}
