package br.com.absn.service.component.session;

import br.com.absn.model.Item;
import br.com.absn.stub.ItemStubRepository;
import br.com.absn.wrapper.ItemCartWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asampaio on 25/03/17.
 */

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CartSession {

    @Autowired
    ItemStubRepository itemRepository;

    private Boolean discountAvailable = true;

    private List<ItemCartWrapper> itemCartList = new ArrayList<ItemCartWrapper>();

    public void add(ItemCartWrapper itemCart){
        itemCartList.add(itemCart);
    }

    public List<ItemCartWrapper> findAll(){
        return itemCartList;
    }

    public void addItemById(Long id, Integer quantity) {
        Item item = itemRepository.findOne(id);
        add(new ItemCartWrapper(item.getId(), item.getName(), item.getDescription(), item.getPrice(), quantity));
    }

    public void cleanCart(){
        itemCartList.clear();
    }

    public Boolean isDiscountAvailable() {
        return discountAvailable;
    }

    public void setDiscountAvailable(Boolean discountAvailable) {
        this.discountAvailable = discountAvailable;
    }

    public void applyDiscount(){
        for (Item item : itemCartList){
            item.setPrice(
                    item.getPrice()
                            .subtract(item.getPrice()
                                    .multiply(new BigDecimal("5"))
                                    .divide(new BigDecimal(100))));
        }
    }
}
