package br.com.absn.service.component;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by asampaio on 02/04/17.
 */
@Component
public class DateUtil {

    public String formatDateForMoip(Date date) {
        Instant instant = date.toInstant();
        LocalDate localDate = instant.atZone(ZoneId.systemDefault()).toLocalDate();
        return localDate.toString();
    }
}