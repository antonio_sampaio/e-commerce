package br.com.absn.service;

import br.com.absn.wrapper.CustomerWrapper;
import br.com.absn.wrapper.PaymentWrapper;
import br.com.moip.request.PaymentRequest;
import br.com.moip.request.PhoneRequest;
import org.springframework.stereotype.Service;

/**
 * Created by asampaio on 01/04/17.
 */

@Service
public class PhoneService{

    public PhoneRequest makePhoneRequest(CustomerWrapper customerWrapper) throws Exception {
        return new PhoneRequest()
                .setAreaCode(customerWrapper.getPhone().getAreaCode().toString())
                .setNumber(customerWrapper.getPhone().getNumber().toString());
    }

    public PhoneRequest makePhoneRequest(PaymentWrapper PaymentWrapper) throws Exception {
        return new PhoneRequest()
                .setAreaCode(PaymentWrapper.getCreditCardWrapper().getHolderWrapper().getPhoneWrapper().getAreaCode().toString())
                .setNumber(PaymentWrapper.getCreditCardWrapper().getHolderWrapper().getPhoneWrapper().getNumber().toString());
    }
}
