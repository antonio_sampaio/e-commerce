package br.com.absn.service;

import br.com.absn.service.component.DateUtil;
import br.com.absn.wrapper.PaymentWrapper;
import br.com.moip.request.CreditCardRequest;
import br.com.moip.request.FundingInstrumentRequest;
import br.com.moip.request.HolderRequest;
import br.com.moip.request.TaxDocumentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by asampaio on 02/04/17.
 */

@Service
public class FundingInstrumentService {

    @Autowired
    private DateUtil dateUtil;

    @Autowired
    private PhoneService phoneService;

    public FundingInstrumentRequest makeFundingInstrument(PaymentWrapper paymentWrapper) throws Exception {
        return new FundingInstrumentRequest().creditCard(makeCreditCardRequest(paymentWrapper));
    }

    private CreditCardRequest makeCreditCardRequest(PaymentWrapper paymentWrapper) throws Exception {
        return new CreditCardRequest()
                .hash(paymentWrapper.getCreditCardWrapper().getHash())
                .holder(new HolderRequest()
                        .birthdate(dateUtil.formatDateForMoip(paymentWrapper.getCreditCardWrapper().getHolderWrapper().getBirthdate()))
                        .fullname(paymentWrapper.getCreditCardWrapper().getHolderWrapper().getFullName())
                        .phone(phoneService.makePhoneRequest(paymentWrapper))
                        .taxDocument(TaxDocumentRequest.cpf(paymentWrapper.getCreditCardWrapper().getHolderWrapper().getCpf().toString()))
                );
    }

}
