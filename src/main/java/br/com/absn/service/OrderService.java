package br.com.absn.service;

import br.com.absn.config.MoipApi;
import br.com.absn.request.SubTotalsRequest;
import br.com.absn.service.component.session.CartSession;
import br.com.absn.wrapper.*;
import br.com.moip.request.*;
import br.com.moip.resource.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by asampaio on 18/03/17.
 */

@Service
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class OrderService {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private MoipService moipService;

    @Autowired
    private CartSession cartSession;

    public Order createOrder(OrderWrapper orderWrapper) throws Exception {
        return moipService.createOrder(makeOrderRequest(orderWrapper));
    }

    private OrderRequest makeOrderRequest(OrderWrapper orderWrapper) throws Exception {
        CustomerRequest customerRequest = customerService.makeCustomerRequest(orderWrapper.getCustomer());

        OrderRequest orderRequest = new OrderRequest()
                .ownId("2")
                .customer(customerRequest);

        addItemOrderRequest(orderRequest, orderWrapper.getCartWrapper());

        return orderRequest;
    }

    private void addItemOrderRequest(OrderRequest orderRequest, CartWrapper cartWrapper){
        for (ItemCartWrapper item:cartWrapper.getItens()){
            orderRequest.addItem(item.getName(), item.getQuantity(), item.getDescription(), (item.getPrice()).intValue());
        }
    }

    public OrderWrapper makeOrderWrapper(
            String fullname,
            String email,
            String birthDate,
            String cpf,
            String phoneAreaCode,
            String phoneNumber,
            String street,
            String streetNumber,
            String complement,
            String district,
            String city,
            String state,
            String country,
            String zipCode) throws ParseException {

        PhoneWrapper phoneWrapper = new PhoneWrapper(Integer.parseInt(phoneAreaCode), Integer.parseInt(phoneNumber));

        ShippingAddressWrapper shippingAddressWrapper = new ShippingAddressWrapper(
                street,
                Integer.parseInt(streetNumber),
                complement,
                city,
                state,
                district,
                country,
                Integer.parseInt(zipCode));

        CustomerWrapper customerWrapper = new CustomerWrapper(
                fullname+email,
                fullname, email,
                new SimpleDateFormat("yyyy-MM-dd").parse(birthDate),
                Long.parseLong(cpf),
                phoneWrapper,
                shippingAddressWrapper
        );

        CartWrapper cartWrapper = new CartWrapper(cartSession.findAll());

        return new OrderWrapper(
                new Date().toString(),
                cartWrapper,
                customerWrapper
        );
    }

}
