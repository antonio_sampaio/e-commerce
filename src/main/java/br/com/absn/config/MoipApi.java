package br.com.absn.config;

import br.com.moip.API;
import br.com.moip.Client;
import br.com.moip.authentication.Authentication;
import br.com.moip.authentication.BasicAuth;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by asampaio on 18/03/17.
 */

@Component
public class MoipApi {

    @Value("01010101010101010101010101010101")
    private String token;

    @Value("ABABABABABABABABABABABABABABABABABABABAB")
    private String key;

    public API getApi() {
        return new API(getClient());
    }

    private Client getClient() {
        return new Client(Client.SANDBOX, getAuthentication());
    }

    private Authentication getAuthentication() {
        return new BasicAuth(token, key);
    }
}
