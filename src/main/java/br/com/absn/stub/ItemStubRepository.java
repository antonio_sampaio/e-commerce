package br.com.absn.stub;

import br.com.absn.model.Item;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by asampaio on 01/04/17.
 */

@Service
public class ItemStubRepository {

    private static Map<Long, Item> itens = new HashMap<Long, Item>();
    private static Long idIndex = 8L;

    static{
        Item item1 = new Item(1L, "ROLEX", "Yacht-Master 40 Dark Rhodium Dial Steel Oyster Mens Watch RSO", new BigDecimal(9995.00));
        itens.put(1L, item1);

        Item item2 = new Item(2L, "OMEGA", "Speedmaster Racing Automatic Chronograph Black Dial Stainless Steel Mens Watch", new BigDecimal(2935.00));
        itens.put(2L, item2);

        Item item3 = new Item(3L, "PANERAI", "Luminor Base Logo Acciaio Mens Hand Wound Watch", new BigDecimal(3750.00));
        itens.put(3L, item3);

        Item item4 = new Item(4L, "TUDOR", "Pelagos Chronometer Automatic Blue Dial Mens Watch", new BigDecimal(3450.00));
        itens.put(4L, item4);

        Item item5 = new Item(5L, "ORIS", "Divers Sixty-Five Automatic Mens Watch", new BigDecimal(1195.00));
        itens.put(5L, item5);

        Item item6 = new Item(6L, "BAUME ET MERCIER", "Baume and Mercier Classima White Dial Brown Leather Mens Watch", new BigDecimal(825.00));
        itens.put(6L, item6);

        Item item7 = new Item(7L, "IWC", "Pilots Mark XVII Automatic Mens Watch", new BigDecimal(3695.00));
        itens.put(7L, item7);

        Item item8 = new Item(8L, "JAEGER LECOULTRE", "Master Silver Dial Leather Mens Watch", new BigDecimal(6475.00));
        itens.put(8L, item8);
    }

    public static List<Item> findAll(){
        return new ArrayList<Item>(itens.values());
    }

    public static Item findOne(Long id) {
        return itens.get(id);
    }


}
