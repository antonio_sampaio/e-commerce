package br.com.absn.stub;

import br.com.absn.model.Coupon;
import br.com.absn.service.component.session.CartSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asampaio on 03/04/17.
 */

@Service
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class CouponStubRepository {

    @Autowired
    private CartSession cartSession;

    private static List<Coupon> coupons = new ArrayList<Coupon>();

    static {
        Coupon coupon1 = new Coupon("ABSGsblaskjfbLKAJHLKDJPIYA", 5);
        coupons.add(coupon1);

        Coupon coupon2 = new Coupon("LKAJjkjsdlfkjhasdklfyeuyAH", 5);
        coupons.add(coupon2);

        Coupon coupon3 = new Coupon("NYAGDetgVACSDEGggdOIWuufhd", 5);
        coupons.add(coupon3);

        Coupon coupon4 = new Coupon("NHSYueyGAGfetGAJShdfjjHAJh", 5);
        coupons.add(coupon4);

        Coupon coupon5 = new Coupon("kjuhagtFATSfgSFtafGAFstdjd", 5);
        coupons.add(coupon5);
    }

    public boolean isCupomValid(String code){
        for(Coupon coupon : coupons){
            if(coupon.getCode().equals(code) && cartSession.isDiscountAvailable()){
                cartSession.setDiscountAvailable(false);
                cartSession.applyDiscount();
                return true;
            }
        }
        return false;
    }

}
