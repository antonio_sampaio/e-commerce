package br.com.absn.model;

import java.math.BigDecimal;

/**
 * Created by asampaio on 25/03/17.
 */

public class Item {

    private Long id;

    private String name;
    private String description;
    private BigDecimal price;

    public Item() {
    }

    public Item(Long id, String name, String description, BigDecimal price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
