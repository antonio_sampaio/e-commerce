package br.com.absn.model;

/**
 * Created by asampaio on 03/04/17.
 */
public class Coupon {

    private String code;
    private Integer discount;

    public Coupon(String code, Integer discount) {
        this.code = code;
        this.discount = discount;
    }

    public String getCode() {
        return code;
    }
}
